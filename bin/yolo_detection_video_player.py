# import the necessary packages
from itertools import starmap
import numpy as np
import recognize as plate
import time as time
import argparse
import imutils
import cv2


# Get the names of the output layers
def getOutputsNames(net):
    # Get the names of all the layers in the network
    layersNames = net.getLayerNames()
    # Get the names of the output layers, i.e. the layers with unconnected outputs
    return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]


# Remove the bounding boxes with low confidence using non-maxima suppression
def postprocess(frame, outs, placa_antiga):
    frameHeight = frame.shape[0]
    frameWidth = frame.shape[1]

    classIds = []
    confidences = []
    boxes = []
    # Scan through all the bounding boxes output from the network and keep only the
    # ones with high confidence scores. Assign the box's class label as the class with the highest score.
    classIds = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > confThreshold:
                center_x = int(detection[0] * frameWidth)
                center_y = int(detection[1] * frameHeight)
                width = int(detection[2] * frameWidth)
                height = int(detection[3] * frameHeight)
                left = int(center_x - width / 2)
                top = int(center_y - height / 2)
                classIds.append(classId)
                confidences.append(float(confidence))
                boxes.append([left, top, width, height])

    # Perform non maximum suppression to eliminate redundant overlapping boxes with
    # lower confidences.
    indices = cv2.dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThreshold)
    for i in indices:
        i = i[0]
        box = boxes[i]
        left = box[0]
        top = box[1]
        width = box[2]
        height = box[3]
        if (classes[classIds[i]] == "car"):
            if ((left + width) - left > 200 or (top + height) - top > 200):
                car = image[top:top + height, left:left + width]
                gray = cv2.cvtColor(car, cv2.COLOR_BGR2GRAY)
                brilhoMedio = np.average(gray)

                (placa, car) = plate.detect_character(car)
                if (placa != ""):
                    placa_antiga = placa
                print("Placa do Carro: {} Média de brilho: {}".format(placa_antiga, brilhoMedio), end="\r")
                drawPred(classIds[i], confidences[i], left, top, left + width, top + height)
        elif (classes[classIds[i]] == "person"):
            drawPred(classIds[i], confidences[i], left, top, left + width, top + height)

    return(placa_antiga)


# Draw the predicted bounding box
def drawPred(classId, conf, left, top, right, bottom):
    # Draw a bounding box.
    cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0))

    label = '%.2f' % conf

    # Get the label for the class name and its confidence
    if classes:
        assert (classId < len(classes))
        label = '%s:%s' % (classes[classId], label)

    # Display the label at the top of the bounding box
    labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)
    top = max(top, labelSize[1])
    cv2.putText(image, label, (left, top), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))


# construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--video", required=True, help="path to input video")
# args = vars(ap.parse_args())

#### Inicializa os parâmetros da Rede Neural ####
confThreshold = 0.5  # Confidence threshold
nmsThreshold = 0.4  # Non-maximum suppression threshold
inpWidth = 416  # Width of network's input image
inpHeight = 416  # Height of network's input image

#### Carregando classes, rede treinada e configurações da rede neural ####

# Load names of classes
classesFile = "/home/marcelovbcfilho/Desktop/yolo-anpr-lava-jato/neural_network/coco.names";
classes = None
with open(classesFile, 'rt') as f:
    classes = f.read().rstrip('\n').split('\n')

# Give the configuration and weight files for the model and load the network using them.
modelConfiguration = "/home/marcelovbcfilho/Desktop/yolo-anpr-lava-jato/neural_network/yolov3.cfg";
modelWeights = "/home/marcelovbcfilho/Desktop/yolo-anpr-lava-jato/neural_network/yolov3.weights";

net = cv2.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

#### Carrega o vídeo recebido como argumento ####
video = cv2.VideoCapture("/home/marcelovbcfilho/Desktop/yolo-anpr-lava-jato/examples/placas_teste/LavaJato1.mp4")
placa_antiga = "XXX1234"

success, image = video.read()

# Loop pelos frames do video
while success:
    # Recupera o próximo frame
    success, image = video.read()

    image = image[100:500, 100:620]

    # Create a 4D blob from a frame.
    blob = cv2.dnn.blobFromImage(image, 1 / 255, (inpWidth, inpHeight), [0, 0, 0], 1, crop=False)

    # Sets the input to the network
    net.setInput(blob)

    # Runs the forward pass to get output of the output layers
    outs = net.forward(getOutputsNames(net))

    # Remove the bounding boxes with low confidence
    placa_antiga = postprocess(image, outs, placa_antiga)

    # Put efficiency information. The function getPerfProfile returns the
    # overall time for inference(t) and the timings for each    of the layers(in layersTimes)
    t, _ = net.getPerfProfile()
    label = 'Inference time: %.2f ms' % (t * 1000.0 / cv2.getTickFrequency())
    cv2.putText(image, label, (0, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

    # Write the frame with the detection boxes
    cv2.imshow("Lava Jato", image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

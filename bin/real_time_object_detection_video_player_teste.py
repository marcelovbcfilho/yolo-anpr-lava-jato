# import the necessary packages
from itertools import starmap

import numpy as np
import recognize as plate
import time as time
import argparse
import imutils
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--video", required=True, help="path to input video")
ap.add_argument("-p", "--prototxt", required=True, help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True, help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.2, help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))
COLORS[7] = 0, 0, 0

# Carrega o vídeo recebido como argumento
video = cv2.VideoCapture(args["video"])

success, image = video.read()
count = 0

# load our serialized model from disk
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
placa_antiga = "XXX1234"

# Loop pelos frames do video
while success:
    # Recupera o próximo frame
    success, image = video.read()
    count += 1

    # load the input image and construct an input blob for the image
    # by resizing to a fixed 300x300 pixels and then normalizing it
    # (note: normalization is done via the authors of the MobileNet SSD
    # implementation)
    (h, w) = image.shape[:2]
    image_garage = image  # [80:h, 150:w]
    blob = cv2.dnn.blobFromImage(cv2.resize(image_garage, (300, 300)), 0.007843, (300, 300), 127.5)

    # pass the blob through the network and obtain the detections and predictions
    net.setInput(blob)
    detections = net.forward()
    # loop over the detections
    for i in np.arange(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > args["confidence"]:
            # extract the index of the class label from the `detections`,
            # then compute the (x, y)-coordinates of the bounding box for
            # the object
            idx = int(detections[0, 0, i, 1])
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            if CLASSES[idx] == "car":
                placa = ""
                # startX += 150
                # endX += 150
                # startY += 80
                # endY += 80

                # Verifica se é o carro principal
                if ((endX - startX) > 200 and (endY - startY) > 200):
                    car = image[startY:endY, startX:endX]
                    gray = cv2.cvtColor(car, cv2.COLOR_BGR2GRAY)
                    brilhoMedio = np.average(gray)

                    # Verifica se o brilho está abaixo de 140(carro sem espuma)
                    #if (brilhoMedio < 130):
                    (placa, car) = plate.detect_character(car)
                    if (placa != ""):
                        placa_antiga = placa
                    print("Placa do Carro: {} Média de brilho: {}".format(placa_antiga, brilhoMedio), end="\r")
                    car = imutils.auto_canny(car)
                    cv2.imshow("Carro", car)
                    cv2.waitKey(0)

                    #mask = np.zeros(frame.shape[:2], np.uint8)
                    #bgdModel = np.zeros((1, 65), np.float64)
                    #fgdModel = np.zeros((1, 65), np.float64)

                    # Se não ele está com espuma
                    #else:
                    #blurred = cv2.GaussianBlur(gray, (5, 5), 0)
                    #cv2.imshow("Gray", gray)
                    #thresh = cv2.threshold(blurred, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
                        # thresh = cv2.erode(thresh, None, iterations=2)
                        # canny = cv2.Canny(thresh, 150, 200)
                        #cnts = cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                        #cnts = cnts[0] if imutils.is_cv2() else cnts[1]
                        # loop over the contours individually
                        #for c in cnts:
                            # construct a mask by drawing only the current contour
                            #cv2.drawContours(gray, [c], -1, (0, 255, 0), 2)

                            # show the images
                            #cv2.imshow("Gray", gray)
                    #cv2.imshow("Carro sozinho", thresh)

                    print("Carro com espuma", end="\r")
                    label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
                    # print("[INFO] {}".format(label))
                    cv2.rectangle(image, (startX, startY), (endX, endY), COLORS[idx], 2)
                    y = startY - 15 if startY - 15 > 15 else startY + 15
                    cv2.putText(image, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                    cv2.imshow("Lava Jato", image)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
